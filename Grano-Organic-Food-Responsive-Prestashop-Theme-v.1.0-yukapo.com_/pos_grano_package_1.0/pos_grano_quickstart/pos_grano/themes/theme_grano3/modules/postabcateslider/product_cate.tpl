{if $productCate }
	<div class='imgSpinner'> </div>	
		<div class="productTabCategorySlider owl-carousel">						
			{foreach from=$productCate item=product name=myLoop}	
				{if ($smarty.foreach.myLoop.index) % $rows == 0 || $smarty.foreach.myLoop.first }
					<div class="item-product">
				{/if}
					<article class="style_product_default product-miniature js-product-miniature item_in" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
						<div class="img_block">
						  {block name='product_thumbnail'}
							<a href="{$product.url}" class="thumbnail product-thumbnail">
							  <img class="first-image lazyload"
								data-src = "{$product.cover.bySize.home_default.url}" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" 
								alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
								data-full-size-image-url = "{$product.cover.large.url}"
							  >
							   {hook h="rotatorImg" product=$product}	
							</a> 
						  {/block}
							<div class="quick-view">
								{block name='quick_view'}
								<a class="quick_view" href="#" data-link-action="quickview" title="{l s='Quick view' d='Shop.Theme.Actions'}">
								 <span>{l s='Quick view' d='Shop.Theme.Actions'}</span>
								</a>
								{/block}
							</div>
						</div>
						<div class="product_desc">
							{block name='product_reviews'}
								<div class="hook-reviews">
								{hook h='displayProductListReviews' product=$product}
								</div>
							{/block} 
							{if isset($product.id_manufacturer)}
							 <div class="manufacturer"><a href="{url entity='manufacturer' id=$product.id_manufacturer }">{Manufacturer::getnamebyid($product.id_manufacturer)}</a></div>
							{/if}
							{block name='product_name'}
							  <h3 itemprop="name"><a href="{$product.url}" class="product_name {if $postheme.name_length ==0 }one_line{/if}" title="{$product.name}">{$product.name|truncate:50:'...'}</a></h3> 
							{/block}
							{block name='product_price_and_shipping'}
							  {if $product.show_price}
								<div class="product-price-and-shipping">
								  {if $product.has_discount}
									{hook h='displayProductPriceBlock' product=$product type="old_price"}

									<span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
									<span class="regular-price">{$product.regular_price}</span>
								  {/if}

								  {hook h='displayProductPriceBlock' product=$product type="before_price"}

								  <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
								  <span itemprop="price" class="price {if $product.has_discount}price-sale{/if}">{$product.price}</span>
								  {hook h='displayProductPriceBlock' product=$product type='unit_price'}

								  {hook h='displayProductPriceBlock' product=$product type='weight'}
									{if $product.has_discount}
										{if $product.discount_type === 'percentage'}
										  <span class="discount-percentage discount-product">{$product.discount_percentage}</span>
										{elseif $product.discount_type === 'amount'}
										  <span class="discount-amount discount-product">{$product.discount_amount_to_display}</span>
										{/if}
									  {/if}
								</div>
							  {/if}
							{/block} 
											
						</div>
					</article>
					
				{if ($smarty.foreach.myLoop.iteration) % $rows == 0 || $smarty.foreach.myLoop.last  }
					</div>
				{/if}
			{/foreach}
		</div>
{/if}
<script>
	var tabcateSlide = $(".tab-category-container-slider .productTabCategorySlider");
	var $tabcateSlideConf = $('.tab-category-container-slider');
	var items       = parseInt($tabcateSlideConf.attr('data-items'));
	var speed     	= parseInt($tabcateSlideConf.attr('data-speed'));
	var autoPlay    = parseInt($tabcateSlideConf.attr('data-autoplay'));
	var time    	= parseInt($tabcateSlideConf.attr('data-time'));
	var arrow       = parseInt($tabcateSlideConf.attr('data-arrow'));
	var pagination  = parseInt($tabcateSlideConf.attr('data-pagination'));
	var move        = parseInt($tabcateSlideConf.attr('data-move'));
	var pausehover  = parseInt($tabcateSlideConf.attr('data-pausehover'));
	var lg          = parseInt($tabcateSlideConf.attr('data-lg'));
	var md          = parseInt($tabcateSlideConf.attr('data-md'));
	var sm          = parseInt($tabcateSlideConf.attr('data-sm'));
	var xs          = parseInt($tabcateSlideConf.attr('data-xs'));
	var xxs         = parseInt($tabcateSlideConf.attr('data-xxs'));
	
	if(autoPlay==1) {
		if(time){
			autoPlay = time;
		}else{
			autoPlay = '3000';
		}
	}else{
		autoPlay = false;
	}
	if(pausehover){ pausehover = true }else{ pausehover=false }
	if(move){ move = false }else{ move=true }
	if(arrow){ arrow =true }else{ arrow=false }
	if(pagination==1){ pagination = true }else{ pagination=false }
	tabcateSlide.owlCarousel({
		autoplay : autoPlay ,
		smartSpeed: speed,
		autoplayHoverPause: pausehover,
		addClassActive: true,
		scrollPerPage: move,
		nav : arrow,
		dots : pagination,
		responsiveClass:true,		
		responsive:{
			0:{
				items:xxs,
			},
			360:{
				items:xs,
			},	
			576:{
				items:sm,
			},
			768:{
				items:md,
			},
			992:{
				items:lg,
			},
			1200:{
				items:items,
			}
		}
	});
	checkClasses();
    tabcateSlide.on('translated.owl.carousel', function(event) {
        checkClasses();
    });

    function checkClasses(){
        tabcateSlide.each(function(){
        	var total = $(this).find('.owl-stage .owl-item.active').length;
			$(this).find('.owl-item').removeClass('firstActiveItem');
			$(this).find('.owl-item').removeClass('lastActiveItem');
			$(this).find('.owl-item.active').each(function(index){
				if (index === 0) { $(this).addClass('firstActiveItem'); }
				if (index === total - 1 && total>1) {
					$(this).addClass('lastActiveItem'); 
				}
			}) 
        });
    }
	$('.pos_content').each(function(){
		$(this).find('.js-product-miniature .product_desc').addClass('match_height');
		matchHeight.init($(this));
		var matchHeightowl = $(this).find('.owl-carousel');
		matchHeightowl.on('resized.owl.carousel.owl.carousel', function(event) {
		  matchHeight.init($(this));
			
		}); 
	});
</script>	
