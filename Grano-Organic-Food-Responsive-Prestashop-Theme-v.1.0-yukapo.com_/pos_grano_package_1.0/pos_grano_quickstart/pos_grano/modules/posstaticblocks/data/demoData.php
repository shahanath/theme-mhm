<?php

class demoData
{
    public function initData()
    {
        $return = true;
        $languages = Language::getLanguages(true);
        $id_shop = Context::getContext()->shop->id;
        $id_hook_nav = (int)Hook::getIdByName('displayNav');
        $id_hook_top = (int)Hook::getIdByName('displayTopColumn');
        $id_hook_home = (int)Hook::getIdByName('displayHome');
        $id_hook_bottom = (int)Hook::getIdByName('displayContainerbottom');
     
        $queries = [
            'INSERT INTO `'._DB_PREFIX_.'pos_staticblock` (`id_pos_staticblock`, `id_hook`, `position`, `name`,`active`) VALUES
                (1, '.$id_hook_nav.', 1, "Static Nav", 1),
                (2, '.$id_hook_top.', 2, "Static Info", 1),
                (3, '.$id_hook_home.', 3, "Home Banner", 1),
                (4, '.$id_hook_bottom.', 4, "Home Banner2", 1)'
        ];

        foreach (Language::getLanguages(true, Context::getContext()->shop->id) as $lang) {
            $queries[] = 'INSERT INTO `'._DB_PREFIX_.'pos_staticblock_lang` (`id_pos_staticblock`, `id_lang`, `content`) VALUES
                (1, '.(int)$lang['id_lang'].', \'<div id="_desktop_static">
				<div class="static-nav"><a href="#" class="store_locator"><i class="las la-map-marker"></i> Store Locator</a> <a href="#" class="track_order"><i class="las la-shipping-fast"></i> Track Your Order</a></div>
				</div>\'),
				(2, '.(int)$lang['id_lang'].', \'<div class="container">
				<div class="static_info">
				<div class="row">
				<div class="col-info col-xs-12 col-sm-6 col-md-6 col-lg-3">
				<div class="box_info">
				<div class="icon_info las la-shipping-fast"></div>
				<div class="txt_info">
				<h2>Free Shipping</h2>
				<p>On all orders over $75.00</p>
				</div>
				</div>
				</div>
				<div class="col-info col-xs-12 col-sm-6 col-md-6 col-lg-3">
				<div class="box_info">
				<div class="icon_info las la-globe-americas"></div>
				<div class="txt_info">
				<h2>Free Returns</h2>
				<p>Returns are free within 9 days</p>
				</div>
				</div>
				</div>
				<div class="col-info col-xs-12 col-sm-6 col-md-6 col-lg-3">
				<div class="box_info">
				<div class="icon_info las la-money-bill-wave"></div>
				<div class="txt_info">
				<h2>100% Payment Secure</h2>
				<p>Your payment are safe with us.</p>
				</div>
				</div>
				</div>
				<div class="col-info col-xs-12 col-sm-6 col-md-6 col-lg-3">
				<div class="box_info">
				<div class="icon_info las la-headset"></div>
				<div class="txt_info">
				<h2>Support 24/7</h2>
				<p>Contact us 24 hours a day</p>
				</div>
				</div>
				</div>
				</div>
				</div>
				</div>\'),
				(3, '.(int)$lang['id_lang'].', \'<div class="home-banner">
				<div class="row">
				<div class="col col-md-6 col-xs-12">
				<div class="banner-box"><a href="#"><img src="/pos_grano/img/cms/1_4.jpg" alt="" /></a></div>
				</div>
				<div class="col col-md-6 col-xs-12">
				<div class="banner-box"><a href="#"><img src="/pos_grano/img/cms/2_4.jpg" alt="" /></a></div>
				</div>
				</div>
				</div>\'),
				(4, '.(int)$lang['id_lang'].', \'<div class="home-banner">
				<div class="banner-box"><a href="#"><img src="/pos_grano/img/cms/4_1.jpg" alt="" /></a></div>
				</div>\')'
            ;
        }

        $queries[] = 'INSERT INTO `'._DB_PREFIX_.'pos_staticblock_shop` (`id_pos_staticblock`, `id_shop`) VALUES
                (1, 1),
                (2, 1),
                (3, 1),
                (4, 1)'; 

        foreach ($queries as $query) {
            $return &= Db::getInstance()->execute($query);
        }

        return $return;
    }
}
?>