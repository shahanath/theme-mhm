<?php

class demoDataFooter
{
    public function initData()
    {
        $return = true;
        $languages = Language::getLanguages(true);
        $id_shop = Context::getContext()->shop->id;
		$id_hook_footerbefore = (int)Hook::getIdByName('displayFooterBefore');
        $id_hook_footer = (int)Hook::getIdByName('displayFooter');
        $id_hook_footerafter = (int)Hook::getIdByName('displayFooterAfter');
        $queries = [
            'INSERT INTO `'._DB_PREFIX_.'posstaticfooter` (`id_posstaticfooter`, `id_hook`, `position`, `width`, `type_content`, `name_module`, `content`,`active_title`) VALUES
                (1, '.$id_hook_footerbefore.', 1, 9, 1, \'ps_emailsubscription\', \'{"cms":[false],"product":[false],"static":[false]}\', 0),
			    (2, '.$id_hook_footerbefore.', 2, 3, 1, 0, \'{"cms":[false],"product":[false],"static":[false]}\', 0),
				(3, '.$id_hook_footer.', 3, 12, 1,0, \'{"cms":[false],"product":[false],"static":[false]}\', 0),
                (4, '.$id_hook_footer.', 4, 4, 1,0, \'{"cms":[false],"product":[false],"static":[false]}\', 0),
                (5, '.$id_hook_footer.', 5, 2, 0, 0, \'{"cms":["1","4","5"],"product":[false],"static":["contact","sitemap","stores"]}\', 1),
				(6, '.$id_hook_footer.', 6, 2, 0, 0, \'{"cms":["2"],"product":["prices-drop","new-products","best-sales"],"static":["authentication","my-account"]}\', 1),
				(7, '.$id_hook_footer.', 7, 2, 1, \'ps_customeraccountlinks\', \'{"cms":[false],"product":[false],"static":[false]}\', 1),
				(8, '.$id_hook_footer.', 8, 2, 1, \'ps_socialfollow\', \'{"cms":[false],"product":[false],"static":[false]}\', 1),
				(9, '.$id_hook_footerafter.', 9, 12, 1, 0, \'{"cms":[false],"product":[false],"static":[false]}\', 0);'
        ];

        foreach (Language::getLanguages(true, Context::getContext()->shop->id) as $lang) {
            $queries[] = 'INSERT INTO `'._DB_PREFIX_.'posstaticfooter_lang` (`id_posstaticfooter`, `id_lang`, `name`, `html_content`, `custom_content`) VALUES
                (1, '.(int)$lang['id_lang'].', "Newsletter", "", ""),
				(2, '.(int)$lang['id_lang'].', "App Store",\'<div class="img_app">
					<a href="#"><img src="/pos_grano/img/cms/app_store.png" alt=""></a>
					<a href="#"><img src="/pos_grano/img/cms/google_play.png" alt=""></a> 
				</div>\', ""),
				(3, '.(int)$lang['id_lang'].', "Static Cms",\'<div class="static_cms_footer">
				<div class="row">
				<div class="col-cms col-md-6 col-lg-3">
				<div class="box_cms"><img src="/pos_grano/img/cms/icon_cms1.png" alt="" class="img-responsive" />
				<div class="txt_cms">
				<h2>100% SECURE PAYMENTS</h2>
				<p>Moving your card details to a much more secured place</p>
				</div>
				</div>
				</div>
				<div class="col-cms col-md-6 col-lg-3">
				<div class="box_cms"><img src="/pos_grano/img/cms/icon_cms2.png" alt="" class="img-responsive" />
				<div class="txt_cms">
				<h2>TRUSTPAY</h2>
				<p>100% Payment Protection. Easy Return Policy</p>
				</div>
				</div>
				</div>
				<div class="col-cms col-md-6 col-lg-3">
				<div class="box_cms"><img src="/pos_grano/img/cms/icon_cms3.png" alt="" class="img-responsive" />
				<div class="txt_cms">
				<h2>HELP CENTER</h2>
				<p>GGot a question? Look no further.Browse our FAQs or submit your query here.</p>
				</div>
				</div>
				</div>
				<div class="col-cms col-md-6 col-lg-3">
				<div class="box_cms"><img src="/pos_grano/img/cms/icon_cms4.png" alt="" class="img-responsive" />
				<div class="txt_cms">
				<h2>Express Shipping</h2>
				<p>Fast, reliable delivery from global warehouses</p>
				</div>
				</div>
				</div>
				</div>
				</div>\', ""),
				(4, '.(int)$lang['id_lang'].', "About Us",\'<div class="footer_about_us">
				<div class="logo_footer">
					<a href="#"><img src="/pos_grano/img/cms/logo_footer.png" alt=""></a>
				</div>
				<div class="desc_info">
					<p class="txt_info">We are a team of designers and developers that create high quality Magento, Prestashop, Opencart.</p>
					<p class="address add">
						<span>4710-4890 Breckinridge USA</span>
					</p>
					<p class="email add">
						<a href="#">demo@posthemes.com</a>
					</p>
					<p class="phone add">
						<span>+1 123 888 9999</span>
					</p>
				</div>
				</div>\', ""),
                (5, '.(int)$lang['id_lang'].', "Information", "", ""),
				(6, '.(int)$lang['id_lang'].', "Custom Links", "", ""),				
				(7, '.(int)$lang['id_lang'].', "My Account", "", ""),				
				(8, '.(int)$lang['id_lang'].', "CONNECT WITH US", \'<div class="payment">
					<img src="/pos_grano/img/cms/payment.png" alt="">
				</div>\', ""),
				(9, '.(int)$lang['id_lang'].', "Copyright Block", \'<div class="copyright">Copyright © <a href="http://posthemes.com/">Posthemes</a>. All Rights Reserved</div>\', "")'
				
            ;
        }

        $queries[] = 'INSERT INTO `'._DB_PREFIX_.'posstaticfooter_shop` (`id_posstaticfooter`, `id_shop`) VALUES
                (1, 1),
                (2, 1),
                (3, 1),
                (4, 1),
                (5, 1),
                (6, 1),
                (7, 1),
                (8, 1),
                (9, 1)';

        foreach ($queries as $query) {
            $return &= Db::getInstance()->execute($query);
        }

        return $return;
    }
}
?>